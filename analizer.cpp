#include "analizer.hpp"

Analizer::Analizer(std::shared_ptr<Params> params) : 
    _params(params),
    _lowCounter(0), 
    _highCounter(0) {
}

void Analizer::process(std::string block) {

    double sumation = 0.0;
    double blocksize = static_cast<double>( _params->getBlocksize() );

    std::vector<double> counter(256);
    counter.assign(256, 0.0);

    //count each byte of block
    std::for_each(block.begin(), block.end(), [&](char ch){ counter[ch]++; });

    //sumation
    std::for_each(counter.begin(), counter.end(), 
        [&](double ch) { sumation += ch==0.0 ? 0.0 : ((ch/blocksize) * log2(ch/blocksize)); } );

    //result
    _blockResults.push_back(-1.0 * sumation);

    //count highers
    if ( sumation > HIGHENTROPY ) {
        _highCounter++;
    }
    //count lowers
    else if ( sumation < LOWENTROPY ) {
        _lowCounter++;
    }
}

void Analizer::printResult() {

    std::cout << std::endl;
    std::cout << "entropy report for " << _params->getFilename() << std::endl; 
    std::cout << "block#  entropy" << std::endl;

    int block = 0;
    std::for_each(_blockResults.begin(), _blockResults.end(), [&](double result) { 
        std::cout << "    " << block++ << "   " << std::setprecision(2) << result << std::endl; });

    std::cout << std::endl;
    std::cout << "Low entropy blocks: " << _lowCounter << std::endl; 
    std::cout << "High entropy blocks: " << _highCounter << std::endl;
}