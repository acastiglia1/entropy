#include "reader.hpp"

Reader::Reader(std::shared_ptr<Params> params) : 
    _params(params), 
    _buffer(params->getBlocksize(), '\0'),
    _infile(params->getFilename(), std::ifstream::in | std::ifstream::binary) {

}

Reader::~Reader() { 

    _infile.close();
}

std::string Reader::getBuffer() {

    std::memset( &_buffer[0], '\0', _params->getBlocksize());

    _infile.read(&_buffer[0], _params->getBlocksize());

    if ( _infile.eof() ) {
    
        auto partial = _buffer.find('\0');

        if ( partial != std::string::npos ) {
        
            _buffer.resize(_buffer.find('\0'));
        }
    }

    return _buffer;
}