#pragma once

#include <cstring>
#include <fstream>
#include <cmath>
#include <memory>
#include <ostream>
#include <sstream>

class Params {

private:
    Params(int argc, char** argv);

    std::string usage() const;

    int _argc; 
    char** _argv;

    std::string _filename;
    int _blocksize;
    int _filesize;

public:
    static std::shared_ptr<Params> parse(int argc, char** argv);

    std::string getFilename();
    int getBlocksize();
    int getFilesize();
    int getNumberOfBlocks();
};