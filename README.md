## Entropy measurement tool
Create a small command-line tool called **entropy** that analyzes a single file and outputs some entropy metrics. It should split the file into small blocks and calculate a single entropy
index for each block.<br/>
This tool can be used to identify compressed and uncompressed areas within the file.
## Arguments
```
entropy [ -b <blocksize> ] <filename>

  <filename> is the path of the file to analyze
  -b <blocksize> optional argument that specifies the block-size in bytes. The default block-size is 1024 bytes.
```
## Calculation
The entropy for a block can be calculated as <br/>
```math
e=-\sum_{for\space every\space byte\space b} P_b \cdot \log_2 P_b
```
Where $`P_b`$ is the relative frequency of byte b within the block 
```math
P_b=\frac{number\space of\space byte\space present}{block\space size}
```

Take a look at https://en.wikipedia.org/wiki/Entropy_(information_theory) to get a more
detailed description of the information theoretical idea of entropy.
## Output
The tool should include a detailed report and a summary report.<br/><br/>
Detailed report:<br/>
For every block, the tool should output the block number as well as the block entropy.<br/><br/>
Summary report:<br/>
In addition, the tool should output, the number of blocks with low entropy (< 2) and high entropy (> 7). <br/>
## Sample output
```
./entropy sample.dat
entropy report for sample.dat
block# entropy
    0     0.19
    1     2.44
    2     7.89
    3     7.87
    4     7.92
    5     7.95
Low entropy blocks:  1
High entropy blocks: 4
```