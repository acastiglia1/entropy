#pragma once

#include <algorithm>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <memory>
#include <string>
#include <vector>

#include "params.hpp"

#define LOWENTROPY 2
#define HIGHENTROPY 7

class Analizer {

private:
    std::shared_ptr<Params> _params;

    std::vector<double> _blockResults;
    int _lowCounter;
    int _highCounter;

public:
    Analizer(std::shared_ptr<Params> params);

    void process(std::string block);

    void printResult();
};