#pragma once

#include <memory>

#include "params.hpp"

class Reader {

private:
    std::shared_ptr<Params> _params;
    std::ifstream _infile;
    std::string _buffer;

public:
    Reader(std::shared_ptr<Params> params);
    ~Reader();

    std::string getBuffer();
};