#include "entropy.hpp" 

int main(int argc, char** argv) {

    try { 

        auto params = Params::parse(argc, argv);

        Reader reader(params);

        Analizer analizer(params);

        auto buffer = reader.getBuffer();

        while (buffer.size() > 0 ) {

            analizer.process(buffer);

            buffer = reader.getBuffer();
        }

        analizer.printResult();
    }
    catch (const std::string& message) {

        std::cerr << message << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}