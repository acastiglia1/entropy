#include "params.hpp" 

Params::Params(int argc, char** argv) : 
    _argc(argc), _argv(argv), _blocksize(1024) {

    switch ( argc ) {

    case 2:
        _filename = _argv[1];
        break;

    case 4:
        if ( std::memcmp(_argv[1], "-b", 2) == 0 ) { 

            _blocksize = atoi(_argv[2]);
            _filename = _argv[3];
        }
        else if ( std::memcmp(_argv[2], "-b", 2) == 0 ) {

            _filename = _argv[1];
            _blocksize = atoi(_argv[3]);
        }
    }
}

std::string Params::usage() const { 

    std::stringstream text;

    text << _argv[0] << " [-b <blocksize>] <filename>" << std::endl
        << "\t<filename> is the path of the file to analyze" << std::endl
        << "\t-b <blocksize> optional argument that specifies the block-size in bytes. The default block-size is 1024 bytes.";

    return text.str();
}

std::shared_ptr<Params> Params::parse(int argc, char** argv) {

    std::shared_ptr<Params> instance(new Params(argc, argv));

    std::ifstream infile(instance->getFilename(), 
        std::ifstream::in | std::ifstream::binary | std::ifstream::ate );
    
    instance->_filesize = static_cast<int>( infile.tellg() );

    infile.close();

    if ( instance->_filesize < 0 || instance->getBlocksize() < 1 ) {

        throw instance->usage();
    }

    return instance;
}

std::string Params::getFilename() {
    return _filename;
}

int Params::getBlocksize() {
    return _blocksize;
}

int Params::getFilesize() {
    return _filesize;
}

int Params::getNumberOfBlocks() {
    return ceil( static_cast<double>(_filesize) / static_cast<double>(_blocksize) );
}